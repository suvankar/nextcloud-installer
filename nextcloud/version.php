<?php 
$OC_Version = array(22,2,0,1);
$OC_VersionString = '22.2.0 RC2';
$OC_Edition = '';
$OC_Channel = 'stable';
$OC_VersionCanBeUpgradedFrom = array (
  'nextcloud' => 
  array (
    '21.0' => true,
    '22.0' => true,
    '22.1' => true,
  ),
  'owncloud' => 
  array (
    '10.5' => true,
  ),
);
$OC_Build = '2021-09-23T10:19:45+00:00 c78148c57a5f547ea5106f353469e33c170b41c9';
$vendor = 'nextcloud';
